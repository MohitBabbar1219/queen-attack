# Represents a piece from chess which can move and kill other pieces that are present in the same row, column or diagonal
class Queen
  attr_reader :row_index, :column_index

  def initialize(row_index, column_index)
    @row_index = row_index
    @column_index = column_index
  end

  def can_kill?(other_queen)
    (row_index - other_queen.row_index).abs == (column_index - other_queen.column_index).abs || # both on diagonal
      column_index == other_queen.column_index || # both on same column
      row_index == other_queen.row_index || # both on same row
      false
  end
end