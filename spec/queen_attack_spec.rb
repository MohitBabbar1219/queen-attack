require './lib/queen_attack'
require 'rspec'

describe 'Queen' do
  describe '#can_kill' do
    it 'expects false given white queen is at (2, 4) and black queen is at (6, 6)' do
      white_queen = Queen.new(2, 4)
      black_queen = Queen.new(6, 6)
      expect(white_queen.can_kill?(black_queen)).to eq false
    end

    it 'expects true given white queen is at (2, 4) and black queen is at (2, 6)' do
      white_queen = Queen.new(2, 4)
      black_queen = Queen.new(2, 6)
      expect(white_queen.can_kill?(black_queen)).to eq true
    end

    it 'expects true given white queen is at (4, 5) and black queen is at (2, 5)' do
      white_queen = Queen.new(4, 5)
      black_queen = Queen.new(2, 5)
      expect(white_queen.can_kill?(black_queen)).to eq true
    end

    it 'expects true given white queen is at (2, 2) and black queen is at (0, 4)' do
      white_queen = Queen.new(2, 2)
      black_queen = Queen.new(0, 4)
      expect(white_queen.can_kill?(black_queen)).to eq true
    end

    it 'expects true given white queen is at (2, 2) and black queen is at (3, 1)' do
      white_queen = Queen.new(2, 2)
      black_queen = Queen.new(3, 1)
      expect(white_queen.can_kill?(black_queen)).to eq true
    end

    it 'expects true given white queen is at (2, 2) and black queen is at (1, 1)' do
      white_queen = Queen.new(2, 2)
      black_queen = Queen.new(1, 1)
      expect(white_queen.can_kill?(black_queen)).to eq true
    end
  end
end